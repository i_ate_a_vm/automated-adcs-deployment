#####################################
#      ADCS Automated Deployment    #
#####################################

#####################################
#             Assumptions           #
#####################################
# You are running this script from a Domain Controller. 
# You have already installed two Windows Server 2016 machines and both are online.
# Both machines have already had their hostnames changed, remote powershell enabled, and been joined to the domain to match your variables below. HEALTH CHECKS WILL FAIL IF THIS IS NOT THE CASE.

#####################################
#        User-defined variables     #
#####################################
$StandaloneIP = "192.168.1.3"
$EnterpriseIP = "192.168.1.5"
$domainAdminUser = "somewhere.local\Administrator"
$domain = "somewhere"
$domainRoot = "local"
$fileShare = "\\dc-16\share\certs"

#####################################
#     Program-defined variables     #
#####################################
$password = Read-Host -AsSecureString "Enter $domainAdminUser's password" # get password for $domainAdminUser defined above
$domainAdminCred = New-Object System.Management.Automation.PSCredential -ArgumentList $domainAdminUser, $password # cred to use to install CA
$rootCertName = "$domain-Root-Cert.cer"

Write-Host "Connecting to Standalone CA."
Invoke-Command -ComputerName $StandaloneIP -Credential $domainAdminCred -ScriptBlock {

    #####################################
    #        User-defined variables     #
    #          Standalone script        #
    #####################################
    $enterpriseCA = "adcs"
    $domain = "somewhere"
    $domainRoot = "local"
    $user = "somewhere.local\Administrator" # be sure to include domain prefix/ suffix
    $fileShare = "\\dc-16\share\certs"

    # variables below can be left to these defaults, or edited to fit your requirements
    $keyLength = 2048
    $cryptoProvider = "RSA#Microsoft Software Key Storage Provider"
    $hashAlgo = "SHA256"
    $validityPeriodUnits = 10
    $dbLocation = "C:\Windows\system32\CertLog"
    

    #####################################
    #     Program-defined variables     #
    #####################################
    $hostname = hostname
    $rootCertName = "CN=" + "$domain-$hostname" + "-CA, DC=$domain, DC=$domainRoot" # used to export correct root cert
    $entFQDN = "$enterpriseCA" + "." + "$domain" + "." + "$domainRoot" # used to point CDP/ AIA values
    $rootExport = "$fileShare" + "\" + "$domain-root.cer" # used to export root cert to file share for import on domain controller and to build enterprise CA
    $password = Read-Host -AsSecureString "Enter $user's password" # get password for $user defined above
    $caCred = New-Object System.Management.Automation.PSCredential -ArgumentList $user, $password # cred to use to install CA

    #####################################
    #         Begin health checks       #
    #####################################
    #nslookup $entFQDN
    #ping $entFQDN

    #####################################
    #         Begin server config       #
    #####################################
    Write-Output "Beginning ADCS feature installation."
    Install-WindowsFeature ADCS-Cert-Authority -IncludeManagementTools
    Write-Output "Feature installed successfully, beginning ADCS root configuration."
    Install-AdcsCertificationAuthority -Credential $caCred -CAType StandaloneRootCa -CryptoProviderName $cryptoProvider -KeyLength $keyLength -HashAlgorithmName $hashAlgo -ValidityPeriod Years -ValidityPeriodUnits $validityPeriodUnits
    Write-Output "Initial configuration completed successfully. Re-configuring CDPs and AIAs."

    # once the initial config is complete, we need to configure CDP and AIA HTTP locations; I am also removing file URI because I won't need it
    $crllist = "http://<ServerDNSName>/CertEnroll/<CaName><CRLNameSuffix><DeltaCRLAllowed>.crl","file://<ServerDNSName>/CertEnroll/<CaName><CRLNameSuffix><DeltaCRLAllowed>.crl"; foreach ($crl in $crllist) {Remove-CACrlDistributionPoint $crl -Force };
    $aialist = "http://<ServerDNSName>/CertEnroll/<ServerDNSName>_<CaName><CertificateName>.crt","file://<ServerDNSName>/CertEnroll/<ServerDNSName>_<CaName><CertificateName>.crt"; foreach ($aia in $aialist) {Remove-CAAuthorityInformationAccess $aia -Force};

    # re-add http locations using subordinate's information
    Add-CACrlDistributionPoint -Uri "http://$entFQDN/CertEnroll/<CaName><CRLNameSuffix><DeltaCRLAllowed>.crl" -AddToCertificateCdp -AddToFreshestCrl # checks the "includes in CRLs delta CRL locations" and "Include in the CDP extension of issued certs" boxes
    Add-CAAuthorityInformationAccess -Uri "http://$entFQDN/CertEnroll/<ServerDNSName>_<CAName><CertificateName>.crt" -AddToCertificateAia 
    Write-Output "AIA and CDP configured successfully, restarting services."

    # restart ADCS services so all info loads in correctly
    net stop "Active Directory Certificate Services"
    net start "Active Directory Certificate Services"

    # use certutil to change required registry entries
    Write-Output "Re-configuring validity periods, DSConfigDN and DSDomainDN."
    certutil -setreg CA\ValidityPeriod "Years" 
    certutil -setreg CA\ValidityPeriodUnits $validityPeriodUnits
    certutil -setreg CA\DSConfigDN "CN=Configuration,DC=$domain,DC=$domainRoot"
    certutil -setreg CA\DSDomainDN "DC=$domain,DC=$domainRoot"

    # restart ADCS services again to reload reg settings
    Write-Output "Restarting services again."
    net stop "Active Directory Certificate Services"
    net start "Active Directory Certificate Services"

    # publish latest CRLs to "Revoked Certificates"
    Write-Output "Waiting 30 seconds, then publishing first CRL and copying it to a public location." # trying this immediately always causes problems
    Start-Sleep -Seconds 30
    certutil -crl

    # copy .crt and .crl files to subordinate CA via file share - script run on subordinate will capture this data and copy to the appropriate location
    New-PSDrive -name H -PSProvider FileSystem -root $fileShare -Credential $caCred
    Copy-Item -Path C:\Windows\System32\CertSrv\CertEnroll\* H:\ -Force # Copy-Item wasn't playing nice with File Share creds, so we mounted $fileShare to H: instead
    

    # export the root CA certificate 
    Write-Output "Exporting root CA certificate to $rootExport"
    $rootCert = get-childitem -path "Cert:\LocalMachine\Root\" | where -Property "Subject" -eq $rootCertName
    Export-Certificate -Cert $rootCert -FilePath H:\"$domain-Root-Cert.cer"
    Remove-PSDrive -Name H 

    Write-Output "Initial root CA configuration complete."
    }

# import root certificate to AD  
Import-Certificate -FilePath "$fileshare\$rootCertName" -CertStoreLocation Cert:\LocalMachine\Root
Write-Host "Imported Certificate to Root folder on local host."

# begin enterprise CA configuration
Write-Output "Connecting to Enterprise CA."
Invoke-Command -ComputerName $EnterpriseIP -Credential $domainAdminCred -ScriptBlock {

    #####################################
    #        User-defined variables     #
    #          Enterprise script        #
    #####################################
    $domain = "somewhere"
    $fileShare = "\\dc-16\share\certs"
    $user = "somewhere.local\Administrator" # be sure to include domain prefix/ suffix

    #####################################
    #     Program-defined variables     #
    #####################################
    $requestFile = "$fileShare\$domain-cert-request.req"
    $enterpriseCert = "$fileShare\$domain-Enterprise-Cert.cer"
    $rootCertLocation = "$fileShare\$domain-Root-Cert.cer"
    $password = Read-Host -AsSecureString "Enter $user's password" # get password for $user defined above
    $caCred = New-Object System.Management.Automation.PSCredential -ArgumentList $user, $password # cred to use to install CA

    #####################################
    #         Begin server config       #
    #####################################

    # install features
    Write-Host "Beginning feature installation."
    Install-WindowsFeature ADCS-Cert-Authority -IncludeManagementTools
    Write-Host "Feature installed successfully, beginning ADCS enterprise configuration."
    Install-AdcsCertificationAuthority -CAType EnterpriseSubordinateCA -OutputCertRequestFile $requestFile -Credential $caCred
    Write-Output "Initial CA configuration complete. Installing IIS server for AIA/ CRL hosting."
    Install-WindowsFeature -Name Web-Server -IncludeManagementTools
    Write-Output "Web server installed successfully."

    # import root certificate from fileshare location defined in standalone script into Local Machine\Root (or Trusted Root Certification Authorities) 
    Write-Host "Installing root certificate." 
    New-PSDrive -name H -PSProvider FileSystem -root $fileShare -Credential $caCred
    Import-Certificate -FilePath "H:\$domain-Root-Cert.cer" -CertStoreLocation Cert:\LocalMachine\Root
    Remove-PSDrive -Name H

    # submit certificate request
    Write-Host "CA initial configuration complete. Connecting to and requesting certficate from Root CA."
}

# generate certificate for Enterprise CA on Standalone CA
Invoke-Command -ComputerName $StandaloneIP -Credential $domainAdminCred -ScriptBlock {

    #####################################
    #        User-defined variables     #
    #          Enterprise script        #
    #####################################
    $domain = "somewhere"
    $fileShare = "\\dc-16\share\certs"

    #####################################
    #     Program-defined variables     #
    #####################################
    $requestFile = "$fileShare\$domain-cert-request.req"
    $enterpriseCert = "$fileShare\$domain-Enterprise-Cert.cer"

    #####################################
    #         Begin server config       #
    #####################################

    # bug is here - does not allow me to get past either certreq command despite the fact that running them locally works

    Write-Host "Submitting initial request."
    certreq -submit -config "ADCS-Root.somewhere.local\somewhere-ADCS-ROOT-CA" \\dc-16\share\certs\somewhere-cert-request.req \\dc-16\share\certs\somewhere-Enterprise-Cert.cer
    Write-Host "Waiting a few seconds and retrieving cert and exporting to share."
    Start-Sleep -Seconds 5
    certreq -retrieve -config "ADCS-Root.somewhere.local\somewhere-ADCS-ROOT-CA" 14 $enterpriseCert
    Write-Host "Request file written to $requestFile successfully!"
} 

# import certificate for Enterprise CA and publish CRL to accessible web location
Write-Host "Connecting to Enterprise CA to complete configuration." # make sure this is actually completion
Invoke-Command -ComputerName $EnterpriseIP -Credential $domainAdminCred -ScriptBlock {

    #####################################
    #        User-defined variables     #
    #          Enterprise script        #
    #####################################
    $domain = "somewhere"
    $fileShare = "\\dc-16\share\certs"
    $user = "somewhere.local\Administrator" # be sure to include domain prefix/ suffix

    #####################################
    #     Program-defined variables     #
    #####################################
    $requestFile = "$fileShare\$domain-cert-request.req"
    $enterpriseCert = "$fileShare\$domain-Enterprise-Cert.cer"
    $password = Read-Host -AsSecureString "Enter $user's password" # get password for $user defined above
    $caCred = New-Object System.Management.Automation.PSCredential -ArgumentList $user, $password # cred to use to install CA

    #####################################
    #         Begin server config       #
    #####################################

    # import enterprise certificate from fileshare location into Local Machine\Intermediate Certification Authorities
    Write-Host "Installing root certificate." 
    New-PSDrive -name H -PSProvider FileSystem -root $fileShare -Credential $caCred
    Import-Certificate -FilePath "H:\$domain-Root-Cert.cer" -CertStoreLocation Cert:\LocalMachine\CA
    Remove-PSDrive -Name H

    # submit certificate request
    Write-Host "Enterprise CA is fully configured."
}

# post-script tasks
Write-Host "The script has completed successfully. Before you can issue client certs, you must create a GPO publishing your root certificate to the environment."