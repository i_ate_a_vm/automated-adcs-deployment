Automated ADCS Deployment

This script assists with correctly configuring an ADCS infrastructure to use 
a standalone root CA and a subordinate enterprise CA that will rely on the root
cert generated on the root CA. 

SCRIPT:
The script relies heavily on remote powershell and that all servers are domain-joined.
The script is currently set to run off of a domain controller.

ORDER:
After performing basic configuration on your root CA server (see "ASSUMPTIONS:"),
copy the root CA deployment script to your DC and configure ALL variables
within the "User-Defined Variables" blocks throughout the script. Run the script. 
Assuming all your variables were configured correctly, you'll have a fully 
functional ADCS environment once this script completes!

ASSUMPTIONS:
- You have already changed comptuer names on both servers you will run this 
script on.
- You have already joined both CA servers to your domain and configured them to 
use your DC for DNS. 
- You have a basic understanding of the variables you are configuring for your
environment. Defaults will work, but may not be the right choice for your scenario.
- You are running this script from a primary domain controller.
- You have already enabled remote Powershell for both CAs. 

PROJECT TO-DO'S:
- Add more error catching. Or any, really.
- Try to add specific use cases - can we roll out individual certs to each computer,
user, etc.? I think the best first case of this would be client computers
to authenticate to servers with RDP (but only if they're an admin user)

ISSUES:
- My enterprise CA is refusing to allow remote powershell connections.
- After importing certificate created by CSR sent to root CA, the sub server throws an error about not being able to verify cert chain because revocation server is offline.
- Issue above is solved by adding IIS to enterprise host and copying contents of CertEnroll from root CA to defined web server location. Follow this guide, but I had to  allow directory browsing - https://docs.microsoft.com/en-us/windows-server/networking/core-network-guide/cncg/server-certs/configure-web1-to-distribute-certificate-revocation-lists
- Script still needs to import root cert in AD and publish via GPO. Should add a "gpupdate" in Enterprise portion.
- Script does not create web server for Enterprise CA yet.
- Script does not complete Enterprise's cert request from Root CA yet. 
- Script does not complete Enterprise deployment with generated cert yet.